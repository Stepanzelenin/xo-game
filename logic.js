const boardWinningCombinations = [
    [0, 1, 2], [3, 4, 5], [6, 7, 8], [0, 3, 6], 
    [1, 4, 7], [2, 5, 8], [0, 4, 8], [2, 4, 6]];

const boardCurrentPosition = [
    {id:0, value: 'x'}, {id:1, value: 'o'}, {id:2, value: null},
    {id:3, value: 'x'}, {id:4, value: 'o'}, {id:5, value: null},
    {id:6, value: 'x'}, {id:7, value: null}, {id:8, value: null}];

const winnerCheck = (a, b) => {return JSON.stringify(a) === JSON.stringify(b);}
    
let boardPositionsX = boardCurrentPosition.filter(item =>item.value == 'x').map(item=>item.id);
let boardPositionsO = boardCurrentPosition.filter(item =>item.value == 'o').map(item=>item.id);
let winner=false;
let gameResult;
let emptySpaces = boardCurrentPosition.filter(item =>item.value == null);

for (let i = 0; i <= boardWinningCombinations.length; i++) {
    if (winnerCheck(boardPositionsX, boardWinningCombinations[i])){
        winner = "X";
        break;}
    else  if (winnerCheck(boardPositionsO, boardWinningCombinations[i])){
        winner = "O";
        break;}
    else if (i == boardWinningCombinations.length && !emptySpaces.length){
        winner = 'Draw'
    }
}

if (winner != false)
    switch(winner){
        case "X":
            gameResult = 'Игрок X';
            alert('Игрок Х победил!');
            break;
        case "O":
            gameResult = 'Игрок О';
            alert('Игрок О победил!');
            break;
        default:
            gameResult = 'Ничья';
            alert('Ничья');
}