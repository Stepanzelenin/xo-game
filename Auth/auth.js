/*Реализуйте валидацию полей Логин и Пароль на форме авторизации. В поля можно вводить только английские буквы, цифры и знаки: 
точка, нижнее подчеркивание. Введенные значения нельзя копировать из полей. При нажатии на кнопку “Войти”, 
в консоли появляется сообщение в формате: “Логин: <значение_из_поля_логин>, пароль: <значение_из_поля_пароль>” */

const loginButton = document.querySelector('.button');
const loginField = document.getElementById('login-field');
const passwordField = document.getElementById('password-field');

// Отключаем копирование
loginField.addEventListener('copy', (event) => event.preventDefault());
passwordField.addEventListener('copy', (event) => event.preventDefault());

// Проверка вводимых данных
loginField.addEventListener('input', () => {
  loginField.value = loginField.value.replace(/[^a-zA-Z0-9._]/g, '');
});

passwordField.addEventListener('input', () => {
  passwordField.value = passwordField.value.replace(/[^a-zA-Z0-9._]/g, '');
});

// Проверка заполненности полей (отсебятина) и вывод логина пароля в консоль
loginButton.addEventListener('click', () => {
  if (loginField.value === '' || passwordField.value === '') {
    console.error('Логин и пароль должны быть заполнены.');
    return;
  }
  console.log(`Login: ${loginField.value}, password: ${passwordField.value}`);
});
