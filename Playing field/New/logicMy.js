class GameField {
    constructor() {
        // state хранит текущее состояние игрового поля в виде двумерного массива
        this.state = Array(3).fill().map(() => Array(3).fill(''));
        // mode хранит активного игрока ('X' или 'O')
        this.mode = 'X';
        // isOverGame хранит, окончена игра или нет
        this.isOverGame = false;
        // boardWinningCombinations хранит все выйгрышные комбинации
        this.boardWinningCombinations = [
            [0, 1, 2], [3, 4, 5], [6, 7, 8], [0, 3, 6],              
            [1, 4, 7], [2, 5, 8], [0, 4, 8], [2, 4, 6]];
    }

    // getGameFieldStatus возвращает копию текущего состояния
    getGameFieldStatus() {
      return this.state.map(row => [...row]);
    }
  
    // setMode устанавливает активного игрока
    setMode(mode) {
      this.mode = mode;
    }
    
    // FieldCellValue заполняет ячейку игрового поля в заданной строке и столбце символом активного игрока
    // Если ячейка уже занята, выводится предупреждение и метод возвращается, ничего не делая
    // После заполнения ячейки метод проверяет, закончилась ли игра (есть ли победитель) и переключает активного игрока
    fieldCellValue(row, col) {
        // Если игра уже закончилась, возвращаемся, ничего не делая
        if (this.isOverGame) {
            return;
        }
      
        // Если ячейка уже занята, выводим оповещение и возвращаемся, ничего не делая
        if (this.state[row][col] !== '') {
            alert('Эта ячейка уже занята!');
            return;
        }
      
        // Заполняем ячейку символом активного игрока
        this.state[row][col] = this.mode;
        
        // checkWinner проверяет, есть ли победитель или ничья
        this.checkWinner();
        
        // switchMode переключает активного игрока
        this.switchMode();
    }
  
    // switchMode переключает активного игрока
    switchMode() {
        this.mode = this.mode === 'X' ? 'O' : 'X';
    }
  
    // checkWinner проверяет, есть ли в игре победитель или ничья
    // если есть, то отображается предупреждение и игра заканчивается (isOverGame = true)
    checkWinner() {
        // Проверка столбцов
        for (let i = 0; i < 3; i++) {
            if (this.state[i][0] !== '' && this.state[i][0] === this.state[i][1] && this.state[i][0] === this.state[i][2]) {
                this.isOverGame = true;
                alert(`${this.state[i][0]} победил!`);
            }
        }
      
        // Проверка строк
        for (let i = 0; i < 3; i++) {
            if (this.state[0][i] !== '' && this.state[0][i] === this.state[1][i] && this.state[0][i] === this.state[2][i]) {
                this.isOverGame = true;
                alert(`${this.state[0][i]} победил!`);
            }
        }

        // Проверка диагонали (лево верх - право низ)
        if (this.state[0][0] !== '' && this.state[0][0] === this.state[1][1] && this.state[0][0] === this.state[2][2]) {
            this.isOverGame = true;
            alert(`${this.state[0][0]} победил!`);
        }
  
        // Проверка диагонали (лево низ - право верх)
        if (this.state[2][0] !== '' && this.state[2][0] === this.state[1][1] && this.state[2][0] === this.state[0][2]) {
            this.isOverGame = true;
            alert(`${this.state[2][0]} победил!`);
        }
  
        // Проверка ничьей
        if (!this.isOverGame && this.state.every(row => row.every(cell => cell !== ''))) {
            this.isOverGame = true;
            alert('Ничья!');
        }
    }
    
}
  
// Создаем игру
const game = new GameField();
// Играем, пока кто-нибудь не выиграет
const cell00 = document.getElementById("cell-0-0");
cell00.addEventListener("click", function() {
    var img = document.createElement("img");
    console.log(game.mode);
    if (game.mode == 'X') {
        img.src = "./imgs/xxl-x.svg";

        cell00.appendChild(img);
      
        const row = 0;
        const col = 0;
        game.fieldCellValue(row, col);
        console.log(game.getGameFieldStatus()); 
    } else {
        img.src = "./imgs/xxl-zero.svg";

        cell00.appendChild(img);
      
        const row = 0;
        const col = 0;
        game.fieldCellValue(row, col);
        console.log(game.getGameFieldStatus()); 
    }
    

  
});

/*while (!game.isOverGame) {
    // Получаем статус игрового поля
    console.log(game.getGameFieldStatus());

    // Вызываем модалку на ввод строки и столбца
   // const row = parseInt(prompt(`Ходит ${game.mode}. Введи столбец (0-2):`));
  //  const col = parseInt(prompt(`Ходит ${game.mode}. Введи строку (0-2):`));

    // Заполняем ячейку в указанных столбце и строке
    game.fieldCellValue(row, col);
}

// Выводим текущее игровое поле в консоль
console.log(game.getGameFieldStatus());*/
