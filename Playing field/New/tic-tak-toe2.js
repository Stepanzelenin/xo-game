class GameField {
    constructor() {
        // state хранит текущее состояние игрового поля в виде двумерного массива
        this.state = Array(3).fill().map(() => Array(3).fill(''));
        // mode хранит активного игрока ('X' или 'O')
        this.mode = 'X';
        // isOverGame хранит, окончена игра или нет
        this.isOverGame = false;
        // boardWinningCombinations хранит все выйгрышные комбинации
        this.boardWinningCombinations = [
            [0, 1, 2], [3, 4, 5], [6, 7, 8], [0, 3, 6],              
            [1, 4, 7], [2, 5, 8], [0, 4, 8], [2, 4, 6]];
    }

    // getGameFieldStatus возвращает копию текущего состояния
    getGameFieldStatus() {
      return this.state.map(row => [...row]);
    }
  
    // setMode устанавливает активного игрока
    setMode(mode) {
      this.mode = mode;
    }
    
    // FieldCellValue заполняет ячейку игрового поля в заданной строке и столбце символом активного игрока
    // Если ячейка уже занята, выводится предупреждение и метод возвращается, ничего не делая
    // После заполнения ячейки метод проверяет, закончилась ли игра (есть ли победитель) и переключает активного игрока
    FieldCellValue(row, col) {
        // Если игра уже закончилась, возвращаемся, ничего не делая
        if (this.isOverGame) {
            return;
        }
      
        // Если ячейка уже занята, выводим оповещение и возвращаемся, ничего не делая
        if (this.state[row][col] !== '') {
            alert('Эта ячейка уже занята!');
            return;
        }
      
        // Заполняем ячейку символом активного игрока
        this.state[row][col] = this.mode;
        
        // Проверяем, закончилась ли игра (т.е. есть ли победитель)
        this.winnerCheck();
        
        // Переключаем активного игрока
        this.switchMode();
    }
  
    // switchMode переключает активного игрока
    switchMode() {
        this.mode = this.mode === 'X' ? 'O' : 'X';
    }
  
    // checkWinner проверяет, есть ли в игре победитель
    // если есть, то отображается предупреждение и игра заканчивается (isOverGame = true)
    /*checkWinner() {
        // Проверка столбцов
        for (let i = 0; i < 3; i++) {
            if (this.state[i][0] !== '' && this.state[i][0] === this.state[i][1] && this.state[i][0] === this.state[i][2]) {
                this.isOverGame = true;
                alert(`${this.state[i][0]} победил!`);
            }
        }
      
        // Проверка строк
        for (let i = 0; i < 3; i++) {
            if (this.state[0][i] !== '' && this.state[0][i] === this.state[1][i] && this.state[0][i] === this.state[2][i]) {
                this.isOverGame = true;
                alert(`${this.state[0][i]} победил!`);
            }
        }
        if (this.state[0][0] !== '' && this.state[0][0] === this.state[1][1] && this.state[0][0] === this.state[2][2]) {
            this.isOverGame = true;
            alert(`${this.state[0][0]} победил!`);
        }
  
        // Проверка диагонали
        if (this.state[2][0] !== '' && this.state[2][0] === this.state[1][1] && this.state[2][0] === this.state[0][2]) {
            this.isOverGame = true;
            alert(`${this.state[2][0]} победил!`);
        }
  
        // Проверка ничьей
        /*if (!this.isOverGame && this.state.every(row => row.every(cell => cell !== ''))) {
            //this.isOverGame = true;
            alert('It is a draw!');
        } */
}

// Создаем игру
const game = new GameField();

// играть в игру, пока кто-нибудь не выиграет
while (!game.isOverGame) {
    // получить статус игрового поля
    console.log(game.getGameFieldStatus());

    // вызвать модалку на ввод строки и столбца
    const row = parseInt(prompt(`Ходит ${game.mode}. Введи столбец (0-2):`));
    const col = parseInt(prompt(`Ходит ${game.mode}. Введи строку (0-2):`));

    // заполнить ячейку в указанной строке и столбце
    game.FieldCellValue(row, col);
}

// текущее поле выводим в контроль
console.log(game.getGameFieldStatus());

cons.winnerCheck = (a, b) => {return JSON.stringify(a) === JSON.stringify(b);}
    for (let i = 0; i <= game.boardWinningCombinations.length; i++) {
        if (winnerCheck(this.mode.filter(item =>item.value == 'x').map(item=>item.id), game.boardWinningCombinations[i])){
            game.isOverGame = true;
            alert('Игрок Х победил!');
            break;}
        else  if (winnerCheck(this.mode.filter(item =>item.value == 'o').map(item=>item.id), game.boardWinningCombinations[i])){
            game.isOverGame = true;
            alert('Игрок O победил!');
            break;}
        }
    
    /*if (this.isOverGame)
        switch(winner){
            case "X":
                gameResult = 'Игрок X';
                alert('Игрок Х победил!');
                break;
            case "O":
                gameResult = 'Игрок О';
                alert('Игрок О победил!');
                break;
           /* default:
                gameResult = 'Ничья';
                alert('Ничья'); */
    
