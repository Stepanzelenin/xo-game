class GameField {
    constructor() {
        this.state = Array(3).fill().map(() => Array(3).fill(''));
        this.mode = 'X';
        this.isOverGame = false;
        this.boardWinningCombinations = [
            [0, 1, 2], [3, 4, 5], [6, 7, 8], [0, 3, 6],              
            [1, 4, 7], [2, 5, 8], [0, 4, 8], [2, 4, 6]];
    }
  
    getGameFieldStatus() {
        return this.state;
    }
  
    setMode(mode) {
        this.mode = mode;
      }
      
    checkWinningLine(line) {
        const values = line.map(i => this.state[Math.floor(i / 3)][i % 3]);
        if (values.every(v => v === 'X')) {
            return 'X';
        } else if (values.every(v => v === 'O')) {
            return 'O';
        } else {
            return null;
        }
    }
  
    checkWinningLines() {
        for (const line of this.boardWinningCombinations) {
            const winner = this.checkWinningLine(line);
            if (winner) {
                return winner;
            }
        }
        return null;
    }
  
    FieldCellValue(row, col) {
        if (this.isOverGame) {
            alert('Игра окончена!');
            return;
        }
  
        if (this.state[row][col] === '') {
                this.state[row][col] = this.mode;
                this.activePlayer = this.mode === "X" ? "O" : "X";
                const activePlayerImage = this.mode === "X" ? "./imgs/x.svg" : "./imgs/zero.svg";
                const nextPlayerImage = this.activePlayer === "X" ? "./imgs/x.svg" : "./imgs/zero.svg";
                const nextPlayerName = this.activePlayer === "X" ? "Екатерина Плюшкина" : "Владелен Пупкин";
                document.getElementById(`game-step`).innerHTML = `Ходит&nbsp;<img src="${nextPlayerImage}">&nbsp;${nextPlayerName}`;
                const cell = document.getElementById(`cell-${row}-${col}`);
                cell.style.backgroundImage = `url(./imgs/xxl-${this.mode}.svg)`;
                const winner = this.checkWinningLines();
                console.log(this.state);
                if (winner) {
                    this.isOverGame = true;
                    document.getElementById(`game-step`).innerHTML = `Победил&nbsp;<img src="${activePlayerImage}">`;
                } else {
                    this.mode = this.mode === 'X' ? 'O' : 'X';
                }
                } else {
                    alert('Эта ячейка уже занята!');
                }
    }
}
  
const gameField = new GameField();
const cells = document.querySelectorAll('#game-board div');
for (const cell of cells) {
    const [row, col] = cell.id.split('-').slice(1).map(Number);
    cell.addEventListener('click', () => gameField.FieldCellValue(row, col));
}